# Описание

Практическая работа по курсу Архитектура информационных систем выполнена студентами Елисеевым Ильёй и Куприяновым Ильёй.

## Содержание

1. [camera_system](#Камера)
2. [documentation](#Документация)
3. [robot_system](#Робот)
4. [robot_system](#Реализпция серверной части под KOS)


## Камера

Разработан минимальный прототип – программы, которая может читать видео с камеры и показывать на экран при помощи opencv.
Собрае Dockerfile для сборки макета приложения,собран прототип программы в docker

- [Dockerfile](https://gitlab.com/r2674/webcam_robo/-/blob/main/camera_system/Dockerfile)
- [opencv](https://gitlab.com/r2674/webcam_robo/-/blob/main/camera_system/main.py)

## Робот

Реализовано сетевое взаимодействие между камерой и роботом по протоколу UDP. 
- [Без KOS](https://gitlab.com/r2674/webcam_robo/-/tree/main/robot_system)
- [C KOS](https://gitlab.com/r2674/webcam_robo/-/tree/main/robot_system_with_KOS)

## Документация

- [Use-case diagram](https://gitlab.com/r2674/webcam_robo/-/blob/main/documentation/Use-Case/Use-case_model.png)
- [Backlog](https://gitlab.com/r2674/webcam_robo/-/blob/main/documentation/backlog.pdf)
- [Концептуальная модель](https://gitlab.com/r2674/webcam_robo/-/blob/main/documentation/Концептуальная%20схема/concept.png)
- [Диаграмма классов](https://gitlab.com/r2674/webcam_robo/-/blob/main/documentation/Диаграмма%20классов/classes.png)
- [Конечный автомат](https://gitlab.com/r2674/webcam_robo/-/tree/main/documentation/Конечный%20автомат)
- [Диаграмма последовательностей](https://github.com/amirovmir/paintRobot/blob/main/doc/Images/sequence-diagram.png)
